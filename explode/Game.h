#pragma once

#include "SFML/Graphics.hpp"
#include "ParticleSys.h"
#include "Utils.h"
#include "GameObj.h"
#include "MyDB.h"

/*
A box to put Games Constants in.
These are special numbers with important meanings (screen width,
ascii code for the escape key, number of lives a player starts with,
the name of the title screen music track, etc.
*/
namespace GC
{
	//game play related constants to tweak
	const Dim2Di SCREEN_RES{ 1200,800 };
	const float SPEED = 250.f;			//ship speed
	const float SCREEN_EDGE = 0.6f;		//how close to the edge the ship can get
	const char ESCAPE_KEY{ 27 };
	const char BACKSPACE_KEY{ 8 };
	const float ROCK_MIN_DIST = 2.15f;	//used when placing rocks to stop them getting too close
	const int NUM_ROCKS = 50;			//how many to place
	const int PLACE_TRIES = 10;			//how many times to try and place before giving up
	const float ROCK_SPEED = 150.f;
	const Dim2Df ROCK_RAD{ 10.f,40.f };
	const int NUM_BULLETS = 50;
	const int NUM_ENEMIES = 50;
	const float ENEMY_SPEED = 150;
	const float ENEMY_BULLET_SPEED = 400;
	const int NUM_LIVES = 3;
}

struct Metrics {
	const std::string VERSION = "1.4";
	int score;
	int lives;
	std::string name;
	bool useDB = true;
	MyDB db;

	struct PlayerData {
		std::string name;
		int score;
	};
	std::vector<PlayerData> playerData;
	std::string filePath;

	void Restart();
	bool IsScoreInTopTen();
	void SortAndUpdatePlayerData();
	bool Save(const std::string& path = "") {
		return (useDB) ? DBSave(path) : FileSave(path);
	}
	bool Load(const std::string& path, bool _useDB) {
		useDB = _useDB;
		return (useDB) ? DBLoad(path) : FileLoad(path);
	}

	bool FileSave(const std::string& path = "");
	bool FileLoad(const std::string& path);
	bool DBSave(const std::string& path = "");
	bool DBLoad(const std::string& path);
};


/*
Manage the asteroid dodging game
*/
struct Game
{
	sf::Texture texShip;	
	sf::Texture texRock;
	sf::Texture texBullet;
	sf::Texture texEnemy;
	std::vector<GameObj> objects;	//anything moving around
	SpawnTimer rockTimer;
	SpawnTimer enemyTimer;
	float rockShipClearance;	//when placing an asteroid, how many ship lengths away from other rocks should it be, harder = smaller
	ParticleSys particleSys;
	sf::Font font;
	Metrics metrics;
	float timer = 0;
		
	//load textures, create ship and rocks, set all rocks initially inactive
	void Init(sf::RenderWindow& window);
	//move the ship and rocks, spawn new rocks 
	void Update(sf::RenderWindow& window, float elapsed, bool fire, char key);
	//draw everything
	void Render(sf::RenderWindow& window, float elapsed);

	void PlaceRocks(sf::RenderWindow& window, sf::Texture& tex);

	void PlaceExistingRocks(sf::RenderWindow& window);

	enum class Mode { INTRO, GAME, GAME_OVER, ENTER_NAME};
	Mode mode = Mode::INTRO;
	void RenderHUD(sf::RenderWindow& window, float elapsed, sf::Font & font);
	void NewGame(sf::RenderWindow & window);
};

/*
Update every object to see if it is colliding with any other - sets the colliding flag true
objects - any could be colliding
debug - if true, draw the collision radius and mark any collisions in red
*/
void CheckCollisions(std::vector<GameObj>& objects, sf::RenderWindow& window, bool debug = true);
//
void DrawCircle(sf::RenderWindow& window, const sf::Vector2f& pos, float radius, sf::Color col);
/*
file - path and file name and extension
tex - set this up with the texture
*/
bool LoadTexture(const std::string& file, sf::Texture& tex);
/*
Check if two circles are touching
pos1,pos2 - two centres
minDist - minimum colliding distance
*/
bool CircleToCircle(const sf::Vector2f& pos1, const sf::Vector2f& pos2, float minDist);
/*
Test one object against an array of other objects to see if it collides
It's OK if the object happens to be in the array, it won't test against itself
*/
bool IsColliding(GameObj& obj, std::vector<GameObj>& objects);
/*
Setup a new rock to fly in from the right
Look through the objects array, find an inactive rock, pick a new starting position
for it just off screen to the right. Check it is at least extraClearance units away
from anything else and mark active.
If it does collide with something then don't spawn and return false.
*/
bool Spawn(GameObj::ObjectT type, sf::RenderWindow& window, std::vector<GameObj>& objects, float extraClearance);
