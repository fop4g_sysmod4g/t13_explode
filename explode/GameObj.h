#pragma once


#include "SFML/Graphics.hpp"
#include "Utils.h"

struct Game;

struct GameObj
{
	sf::Sprite spr;	//main image
	float radius = 0;	//collision radius
	enum class ObjectT { Ship, Rock, Bullet, Enemy };	//what is this?
	ObjectT type = ObjectT::Rock;
	bool colliding = false; //did we hit something on the last update
	bool active = false;	//should we be updating and rendering this one?
	sf::Vector2f direction{ 0,0 };
	int health = 0;
	Game *pGame = nullptr;
	GameObj *pMySpawner = nullptr;
	SpawnTimer spawner;

	/*
	Call this to setup your object
	window - sfml render window
	tex - texture to use on the sprite
	type - what is it meant to be
	*/
	void Init(sf::RenderWindow& window, sf::Texture& tex, ObjectT type_, Game& game);
	//called by Init as needed
	void InitShip(sf::RenderWindow& window, sf::Texture& tex);
	void ResetShip(sf::RenderWindow& window);
	//called by Init as needed
	void InitRock(sf::RenderWindow& window, sf::Texture& tex);
	void ResetRock();
	void InitEnemy(sf::RenderWindow& window, sf::Texture& tex);
	void ResetEnemy();
	//move and update logic
	void Update(sf::RenderWindow& window, float elapsed, bool fire);
	//draw
	void Render(sf::RenderWindow& window, float elapsed);
	//handle moving the ship around
	void PlayerControl(const sf::Vector2u& screenSz, float elapsed, bool fire);
	//rocks all move left, when leave the left edge of the screen they deactivate
	void MoveRock(float elapsed);

	void MoveBullet(const sf::Vector2u& screenSz, float elapsed);
	void InitBullet(sf::RenderWindow& window, sf::Texture& tex);
	void FireBullet(const sf::Vector2f& pos);

	void MoveEnemy(const sf::Vector2u& screenSz, float elapsed);
	void EnemyShoot(float elapsed);


	void Hit(GameObj& other);
	void TakeDamage(int amount, GameObj& other);
};

