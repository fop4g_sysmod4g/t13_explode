#pragma once
#include <vector>

#include "SFML/Graphics.hpp"
#include "Utils.h"

struct Particle {
	Particle *pNext = nullptr;
	sf::Sprite spr;
	float life = 0;
	sf::Vector2f vel;
};

struct Particles {
	std::vector<Particle> particles;
	Particle *pBusy = nullptr, *pFree = nullptr;
	sf::Texture circle;

	void Init();

	Particle *Remove(Particle *p, Particle *pPrev);

	void Update(float dT);
	bool IsBusy() const {
		return pBusy!=nullptr;
	}
	void Render(sf::RenderWindow& window);
};

struct Emitter {
	sf::Vector2f pos;
	sf::Vector2f scale{ 1,1 };
	sf::Vector2f initVel{ 0,0 };
	Dim2Di initSpeed;
	float rate = 0.01f;
	sf::Color colour = sf::Color::White;
	float life = 1.f;
	int numToEmit = 0;
	int numAtOnce = 1;
	bool alive = false;

	float lastEmit = 0;

	Particle* GetNewParticle(Particles& cache);

	void Update(float dT, Particles& cache);
};

struct ParticleSys {
	Particles cache;
	std::vector<Emitter> emitters;

	void Init();
	void Update(float dT);
	void Render(sf::RenderWindow& window, float dT);
	Emitter* GetNewEmitter();
	int GetNumActiveEmitters() const;
};