#pragma once
#include <string>

//dimensions in 2D that are whole numbers
struct Dim2Di
{
	int x, y;
};

//dimensions in 2D that are floating point numbers
struct Dim2Df
{
	float x, y;
};

struct SpawnTimer {
	float timer=0;				//a clock
	float delay=1;				//how long to wait before another asteroid/enemy comes in, decrease to make harder
	float decayDelay = 0;
	float decayTimer=0;			

	void Reset(float _delay=-1, float _decayDelay=-1) {
		timer = 0;
		if(_delay!=-1)
			delay = _delay;
		if (_decayDelay != -1) {
			decayTimer = 0;
			decayDelay = _decayDelay;
		}
	}

	bool Cycle(float elapsed) {
		timer += elapsed;
		decayTimer += elapsed;
		if (decayDelay>0 && decayTimer >= decayDelay) {
			decayTimer = 0;
			delay *= 0.99f;
		}
		return (timer >= delay);
	}
};

/*
Send text to the debug output window
Second parameter can be ignored
*/
void DebugPrint(const std::string& mssg1, const std::string& mssg2 = "");

